/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "lwip.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#ifndef HSEM_ID_0
#define HSEM_ID_0 (0U) /* HW semaphore 0*/
#endif
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

FDCAN_HandleTypeDef hfdcan2;

osThreadId defaultTaskHandle;
osThreadId myTask02Handle;
osThreadId myTask03Handle;
/* USER CODE BEGIN PV */

//uint8_t TxData[] = {0x10, 0x32, 0x54, 0x76, 0x98, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00,
//		            0x11, 0x31, 0x51, 0x71, 0x91, 0x01, 0x11, 0x21, 0x31, 0x41, 0x51, 0x61, 0x71, 0x81, 0x91, 0x01};

uint8_t TxData[] = {0x10, 0x32, 0x54, 0x76, 0x98, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0x00};
FDCAN_FilterTypeDef sFilterConfig;
FDCAN_RxHeaderTypeDef RxHeader;
uint8_t RxData[16];
FDCAN_TxHeaderTypeDef TxHeader;
uint8_t EthRxData[50];
uint8_t EthRxDataLen;
uint32_t Notification_flag = 0;
uint32_t Ethernet_Notification_flag = 0;
uint32_t Can_Notification_flag=0;

uint8_t *CanMsgID;
uint8_t *CanMsgData;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MPU_Config(void);
static void MX_GPIO_Init(void);
static void MX_FDCAN2_Init(void);
void StartDefaultTask(void const * argument);
void CanFDTask(void const * argument);
void LEDTask(void const * argument);

/* USER CODE BEGIN PFP */
void CanBus_Init(void);
void CanBus_Init2(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
/**
  * @brief This function is called when an UDP datagrm has been received on the port UDP_PORT.
  * @param arg user supplied argument (udp_pcb.recv_arg)
  * @param pcb the udp_pcb which received data
  * @param p the packet buffer that was received
  * @param addr the remote IP address from which the packet was received
  * @param port the remote port from which the packet was received
  * @retval None
  */
void udp_echoserver_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port)
{
  struct pbuf *p_tx;

  /* allocate pbuf from RAM*/
  p_tx = pbuf_alloc(PBUF_TRANSPORT,p->len, PBUF_RAM);

  if(p_tx != NULL)
  {
    pbuf_take(p_tx, (char*)p->payload, p->len);

    //&EthRxData[0] = (uint8_t *)p->payload;
    EthRxDataLen = p_tx->len;

    for (int i=0; i<p_tx->len; i++)
       EthRxData[i] = ((uint8_t*)(p->payload))[i];

    Ethernet_Notification_flag = 1;

//    /* Connect to the remote client */
//    udp_connect(upcb, addr, 55151);
//
//    /* Tell the client that we have accepted it */
//    udp_send(upcb, p_tx);
//
//    /* free the UDP connection, so we can accept new clients */
//    udp_disconnect(upcb);
//
//     Free the p_tx buffer
       pbuf_free(p_tx);
//
    /* Free the p buffer */
       //pbuf_free(p);
  }
}

/**
  * @brief  Initialize the server application.
  * @param  None
  * @retval None
  */
void udp_echoserver_init(void)
{
   struct udp_pcb *upcb;
   err_t err;

   /* Create a new UDP control block  */
   upcb = udp_new();

   if (upcb)
   {
     /* Bind the upcb to the UDP_PORT port */
     /* Using IP_ADDR_ANY allow the upcb to be used by any local interface */
      err = udp_bind(upcb, IP_ADDR_ANY, 55151);

      if(err == ERR_OK)
      {
        /* Set a receive callback for the upcb */
        udp_recv(upcb, udp_echoserver_receive_callback, NULL);
      }
      else
      {
        udp_remove(upcb);
      }
   }
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
/* USER CODE BEGIN Boot_Mode_Sequence_0 */
  int32_t timeout;
/* USER CODE END Boot_Mode_Sequence_0 */

  /* MPU Configuration--------------------------------------------------------*/
  MPU_Config();

  /* Enable I-Cache---------------------------------------------------------*/
  SCB_EnableICache();

  /* Enable D-Cache---------------------------------------------------------*/
  SCB_EnableDCache();

/* USER CODE BEGIN Boot_Mode_Sequence_1 */
  /* Wait until CPU2 boots and enters in stop mode or timeout*/
  timeout = 0xFFFF;
  while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) != RESET) && (timeout-- > 0));
  if ( timeout < 0 )
  {
  Error_Handler();
  }
/* USER CODE END Boot_Mode_Sequence_1 */
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
/* USER CODE BEGIN Boot_Mode_Sequence_2 */
/* When system initialization is finished, Cortex-M7 will release Cortex-M4 by means of
HSEM notification */
/*HW semaphore Clock enable*/
__HAL_RCC_HSEM_CLK_ENABLE();
/*Take HSEM */
HAL_HSEM_FastTake(HSEM_ID_0);
/*Release HSEM in order to notify the CPU2(CM4)*/
HAL_HSEM_Release(HSEM_ID_0,0);
/* wait until CPU2 wakes up from stop mode */
timeout = 0xFFFF;
while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) == RESET) && (timeout-- > 0));
if ( timeout < 0 )
{
Error_Handler();
}
/* USER CODE END Boot_Mode_Sequence_2 */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_FDCAN2_Init();
  /* USER CODE BEGIN 2 */
  CanBus_Init2();

  /* Start the FDCAN module */
  HAL_FDCAN_Start(&hfdcan2);
  HAL_Delay(1000);

  //HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan2, &TxHeader, &TxData[0]);
  //HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan2, &TxHeader, &TxData[8]);

  /* init code for LWIP */
  MX_LWIP_Init();
  udp_echoserver_init();
  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 256);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTask02 */
  //osThreadDef(myTask02, CanFDTask, osPriorityIdle, 0, 128);
  //myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

  /* definition and creation of myTask03 */
  //osThreadDef(myTask03, LEDTask, osPriorityIdle, 0, 128);
  //myTask03Handle = osThreadCreate(osThread(myTask03), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 100;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 20;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief FDCAN2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_FDCAN2_Init(void)
{

  /* USER CODE BEGIN FDCAN2_Init 0 */

  /* USER CODE END FDCAN2_Init 0 */

  /* USER CODE BEGIN FDCAN2_Init 1 */

  /* USER CODE END FDCAN2_Init 1 */
  hfdcan2.Instance = FDCAN2;
  hfdcan2.Init.FrameFormat = FDCAN_FRAME_FD_BRS;
  hfdcan2.Init.Mode = FDCAN_MODE_NORMAL;
  hfdcan2.Init.AutoRetransmission = DISABLE;
  hfdcan2.Init.TransmitPause = DISABLE;
  hfdcan2.Init.ProtocolException = DISABLE;
  hfdcan2.Init.NominalPrescaler = 1;
  hfdcan2.Init.NominalSyncJumpWidth = 1;
  hfdcan2.Init.NominalTimeSeg1 = 2;
  hfdcan2.Init.NominalTimeSeg2 = 2;
  hfdcan2.Init.DataPrescaler = 1;
  hfdcan2.Init.DataSyncJumpWidth = 1;
  hfdcan2.Init.DataTimeSeg1 = 1;
  hfdcan2.Init.DataTimeSeg2 = 1;
  hfdcan2.Init.MessageRAMOffset = 0;
  hfdcan2.Init.StdFiltersNbr = 0;
  hfdcan2.Init.ExtFiltersNbr = 0;
  hfdcan2.Init.RxFifo0ElmtsNbr = 0;
  hfdcan2.Init.RxFifo0ElmtSize = FDCAN_DATA_BYTES_8;
  hfdcan2.Init.RxFifo1ElmtsNbr = 0;
  hfdcan2.Init.RxFifo1ElmtSize = FDCAN_DATA_BYTES_8;
  hfdcan2.Init.RxBuffersNbr = 0;
  hfdcan2.Init.RxBufferSize = FDCAN_DATA_BYTES_8;
  hfdcan2.Init.TxEventsNbr = 0;
  hfdcan2.Init.TxBuffersNbr = 0;
  hfdcan2.Init.TxFifoQueueElmtsNbr = 0;
  hfdcan2.Init.TxFifoQueueMode = FDCAN_TX_FIFO_OPERATION;
  hfdcan2.Init.TxElmtSize = FDCAN_DATA_BYTES_8;
  if (HAL_FDCAN_Init(&hfdcan2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN FDCAN2_Init 2 */

  /* USER CODE END FDCAN2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LD3_Pin */
  GPIO_InitStruct.Pin = LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD3_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void CanBus_Init(void)
{
 /*                Bit time configuration:
	Bit time parameter         | Nominal      |  Data
	---------------------------|--------------|----------------
	fdcan_ker_ck               | 20 MHz       | 20 MHz
	Time_quantum (tq)          | 50 ns        | 50 ns
	Synchronization_segment    | 1 tq         | 1 tq
	Propagation_segment        | 23 tq        | 1 tq
	Phase_segment_1            | 8 tq         | 4 tq
	Phase_segment_2            | 8 tq         | 4 tq
	Synchronization_Jump_width | 8 tq         | 4 tq
	Bit_length                 | 40 tq = 2 µs | 10 tq = 0.5 µs
	Bit_rate                   | 0.5 MBit/s   | 2 MBit/s
	Sample point                  %80            %60
  */
  hfdcan2.Instance 					= FDCAN2;
  hfdcan2.Init.FrameFormat 			= FDCAN_FRAME_FD_BRS;//FDCAN_FRAME_FD_BRS;
  hfdcan2.Init.Mode 				= FDCAN_MODE_NORMAL;
  hfdcan2.Init.AutoRetransmission 	= ENABLE;
  hfdcan2.Init.TransmitPause 		= DISABLE;
  hfdcan2.Init.ProtocolException 	= ENABLE;
  hfdcan2.Init.NominalPrescaler 	= 0x1; 	/* tq = NominalPrescaler x (1/fdcan_ker_ck) */
  hfdcan2.Init.NominalSyncJumpWidth = 0x8;
  hfdcan2.Init.NominalTimeSeg1 		= 0x1F; /* NominalTimeSeg1 = Propagation_segment + Phase_segment_1 */
  hfdcan2.Init.NominalTimeSeg2 		= 0x8;
  hfdcan2.Init.DataPrescaler 		= 0x1;
  hfdcan2.Init.DataSyncJumpWidth 	= 0x4;
  hfdcan2.Init.DataTimeSeg1 		= 0x5; 	/* DataTimeSeg1 = Propagation_segment + Phase_segment_1 */
  hfdcan2.Init.DataTimeSeg2 		= 0x4;
  hfdcan2.Init.MessageRAMOffset 	= 0;
  hfdcan2.Init.StdFiltersNbr 		= 1;
  hfdcan2.Init.ExtFiltersNbr 		= 0;
  hfdcan2.Init.RxFifo0ElmtsNbr 		= 2;
  hfdcan2.Init.RxFifo0ElmtSize 		= FDCAN_DATA_BYTES_8;
  hfdcan2.Init.RxFifo1ElmtsNbr 		= 0;
  hfdcan2.Init.RxBuffersNbr 		= 0;
  hfdcan2.Init.TxEventsNbr 			= 0;
  hfdcan2.Init.TxBuffersNbr 		= 0;
  hfdcan2.Init.TxFifoQueueElmtsNbr 	= 2;
  hfdcan2.Init.TxFifoQueueMode 		= FDCAN_TX_FIFO_OPERATION;
  hfdcan2.Init.TxElmtSize 			= FDCAN_DATA_BYTES_8;
  HAL_FDCAN_Init(&hfdcan2);

  /* Configure Rx filter */
  sFilterConfig.IdType 			= FDCAN_STANDARD_ID;
  sFilterConfig.FilterIndex 	= 0;
  sFilterConfig.FilterType 		= FDCAN_FILTER_MASK;
  sFilterConfig.FilterConfig 	= FDCAN_FILTER_TO_RXFIFO0;
  sFilterConfig.FilterID1	 	= 0x157;
  sFilterConfig.FilterID2 		= 0x7FF; /* For acceptance, MessageID and FilterID1 must match exactly */
  HAL_FDCAN_ConfigFilter(&hfdcan2, &sFilterConfig);

  /* Configure global filter to reject all non-matching frames */
  HAL_FDCAN_ConfigGlobalFilter(&hfdcan2, FDCAN_REJECT, FDCAN_REJECT, FDCAN_REJECT_REMOTE, FDCAN_REJECT_REMOTE);

  /* Configure Rx FIFO 0 watermark to 2 */
  HAL_FDCAN_ConfigFifoWatermark(&hfdcan2, FDCAN_CFG_RX_FIFO0, 2);

  /* Activate Rx FIFO 0 watermark notification */
  HAL_FDCAN_ActivateNotification(&hfdcan2, FDCAN_IT_RX_FIFO0_WATERMARK, 0);

  /* Prepare Tx Header */
  TxHeader.Identifier 			= 0x157;
  TxHeader.IdType 				= FDCAN_STANDARD_ID;
  TxHeader.TxFrameType 			= FDCAN_DATA_FRAME;
  TxHeader.DataLength 			= FDCAN_DLC_BYTES_8;
  TxHeader.ErrorStateIndicator 	= FDCAN_ESI_ACTIVE;
  TxHeader.BitRateSwitch 		= FDCAN_BRS_ON;
  TxHeader.FDFormat 			= FDCAN_FD_CAN;
  TxHeader.TxEventFifoControl 	= FDCAN_NO_TX_EVENTS;
  TxHeader.MessageMarker 		= 0;
}

void CanBus_Init2(void)
{
 /*                Bit time configuration:
	Bit time parameter         | Nominal      |  Data
	---------------------------|--------------|----------------
	Bit_rate                   | 1 MBit/s     | 4 MBit/s
  */
  hfdcan2.Instance 					= FDCAN2;
  hfdcan2.Init.FrameFormat 			= FDCAN_FRAME_FD_BRS;//FDCAN_FRAME_FD_BRS;
  hfdcan2.Init.Mode 				= FDCAN_MODE_NORMAL;
  hfdcan2.Init.AutoRetransmission 	= ENABLE;
  hfdcan2.Init.TransmitPause 		= DISABLE;
  hfdcan2.Init.ProtocolException 	= ENABLE;
  hfdcan2.Init.NominalPrescaler 	= 0x5; 	/* tq = NominalPrescaler x (1/fdcan_ker_ck) */
  hfdcan2.Init.NominalSyncJumpWidth = 0x1;
  hfdcan2.Init.NominalTimeSeg1 		= 0x5; /* NominalTimeSeg1 = Propagation_segment + Phase_segment_1 */
  hfdcan2.Init.NominalTimeSeg2 		= 0x2;
  hfdcan2.Init.DataPrescaler 		= 0x1;
  hfdcan2.Init.DataSyncJumpWidth 	= 0x1;
  hfdcan2.Init.DataTimeSeg1 		= 0x7; 	/* DataTimeSeg1 = Propagation_segment + Phase_segment_1 */
  hfdcan2.Init.DataTimeSeg2 		= 0x2;
  hfdcan2.Init.MessageRAMOffset 	= 0;
  hfdcan2.Init.StdFiltersNbr 		= 1;
  hfdcan2.Init.ExtFiltersNbr 		= 0;
  hfdcan2.Init.RxFifo0ElmtsNbr 		= 2;
  hfdcan2.Init.RxFifo0ElmtSize 		= FDCAN_DATA_BYTES_8;
  hfdcan2.Init.RxFifo1ElmtsNbr 		= 0;
  hfdcan2.Init.RxBuffersNbr 		= 0;
  hfdcan2.Init.TxEventsNbr 			= 0;
  hfdcan2.Init.TxBuffersNbr 		= 0;
  hfdcan2.Init.TxFifoQueueElmtsNbr 	= 2;
  hfdcan2.Init.TxFifoQueueMode 		= FDCAN_TX_FIFO_OPERATION;
  hfdcan2.Init.TxElmtSize 			= FDCAN_DATA_BYTES_8;
  HAL_FDCAN_Init(&hfdcan2);

  /* Configure Rx filter */
  sFilterConfig.IdType 			= FDCAN_STANDARD_ID;
  sFilterConfig.FilterIndex 	= 0;
  sFilterConfig.FilterType 		= FDCAN_FILTER_MASK;
  sFilterConfig.FilterConfig 	= FDCAN_FILTER_TO_RXFIFO0;
  sFilterConfig.FilterID1	 	= 0x169;
  sFilterConfig.FilterID2 		= 0x7FF; /* For acceptance, MessageID and FilterID1 must match exactly */
  HAL_FDCAN_ConfigFilter(&hfdcan2, &sFilterConfig);

  /* Configure global filter to reject all non-matching frames */
  HAL_FDCAN_ConfigGlobalFilter(&hfdcan2, FDCAN_REJECT, FDCAN_REJECT, FDCAN_REJECT_REMOTE, FDCAN_REJECT_REMOTE);

  /* Configure Rx FIFO 0 watermark to 2 */
  HAL_FDCAN_ConfigFifoWatermark(&hfdcan2, FDCAN_CFG_RX_FIFO0, 2);

  /* Activate Rx FIFO 0 watermark notification */
  HAL_FDCAN_ActivateNotification(&hfdcan2, FDCAN_IT_RX_FIFO0_WATERMARK, 0);

  /* Prepare Tx Header */
  TxHeader.Identifier 			= 0x169;
  TxHeader.IdType 				= FDCAN_STANDARD_ID;
  TxHeader.TxFrameType 			= FDCAN_DATA_FRAME;
  TxHeader.DataLength 			= FDCAN_DLC_BYTES_8;
  TxHeader.ErrorStateIndicator 	= FDCAN_ESI_ACTIVE;
  TxHeader.BitRateSwitch 		= FDCAN_BRS_ON;
  TxHeader.FDFormat 			= FDCAN_FD_CAN;
  TxHeader.TxEventFifoControl 	= FDCAN_NO_TX_EVENTS;
  TxHeader.MessageMarker 		= 0;
}


/* Callback function to handle recv messages */
void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs)
{
  if((RxFifo0ITs & FDCAN_IT_RX_FIFO0_WATERMARK) != RESET)
  {
	  HAL_FDCAN_GetRxMessage(&hfdcan2, FDCAN_RX_FIFO0, &RxHeader, RxData);
	  Can_Notification_flag = 1;
  }
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* init code for LWIP */
  //MX_LWIP_Init();
  /* USER CODE BEGIN 5 */
  const char* message;
  char delim[] = "&";
  char delim2[] = "-";
  uint32_t can_id = 0;
  uint8_t CanMsgIDx[8];
  uint32_t test = 0;

  osDelay(10);

	ip_addr_t PC_IPADDR;
	IP_ADDR4(&PC_IPADDR, 192, 168, 100, 2);

	struct udp_pcb* my_udp = udp_new();
	//udp_connect(my_udp, &PC_IPADDR, 55151);
	struct pbuf* udp_buffer = NULL;

  /* Infinite loop */
  for (;;)
  {
    osDelay(10);

    if (Can_Notification_flag)
    {
    	char * new_str ;
		/*new_str[0] = '\0';   // ensures the memory is an empty string

    	message = "ID:";
		strcat(new_str, message);

		message = RxHeader.Identifier;
		strcat(new_str, message);*/

		message = "DATA:";
		strcat(new_str, message);

		message = RxData;
		strcat(new_str, message);

		message = new_str;
    	//message = strtol((uint8_t*)RxData, NULL, 16);

      udp_buffer = pbuf_alloc(PBUF_TRANSPORT, strlen(message), PBUF_RAM);
      if (udp_buffer != NULL)
      {
    	  udp_connect(my_udp, &PC_IPADDR, 55151);
        memcpy(udp_buffer->payload, message, strlen(message));
        udp_send(my_udp, udp_buffer);
        udp_disconnect(my_udp);
        pbuf_free(udp_buffer);
      }

      Can_Notification_flag = 0;
    }


    if (Ethernet_Notification_flag)
    {

		  TxHeader.IdType = FDCAN_STANDARD_ID;
		  TxHeader.TxFrameType = FDCAN_DATA_FRAME;
		  TxHeader.DataLength = FDCAN_DLC_BYTES_8;
		  TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
		  TxHeader.BitRateSwitch = FDCAN_BRS_ON;
		  TxHeader.FDFormat = FDCAN_FD_CAN;
		  TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;
		  TxHeader.MessageMarker = 0;

    	char *ptr = strtok(EthRxData, delim);
    	int idx =0;

    	while(ptr != NULL)
    	{
    		if (idx == 0)
    		{
    			CanMsgID = (uint8_t*)ptr;
//    			can_id = ((uint8_t)CanMsgID[7]) + ((uint8_t)CanMsgID[6] << 8) + ((uint8_t)CanMsgID[5] << 16) + ((uint8_t)CanMsgID[4] << 24) +
//    					((uint8_t)CanMsgID[3] << 32) + ((uint8_t)CanMsgID[2] << 40) + ((uint8_t)CanMsgID[1] << 48) + ((uint8_t)CanMsgID[0] << 56);
//CanMsgID : 0x00000112

    			for (int i=0; i<8; i++)
    				CanMsgIDx[i] = CanMsgID[i];

    			//test = atoi(CanMsgIDx);

    			uint32_t can_id = strtol(CanMsgIDx, NULL, 16);

    			TxHeader.Identifier = can_id;
    		}
    		else if (idx == 1)
    		{
    			CanMsgData = (uint8_t*)ptr;

    	    	char *ptr2 = strtok(CanMsgData, delim2);
    	    	int idx2 = 0;

    	    	while(ptr2 != NULL)
    	    	{
					switch (idx2) {
						case 0:
							TxData[0] = strtol((uint8_t*)ptr2, NULL, 16);//atoi((uint8_t*)ptr2);
							break;
						case 1:
							TxData[1] = strtol((uint8_t*)ptr2, NULL, 16);
							break;
						case 2:
							TxData[2] = strtol((uint8_t*)ptr2, NULL, 16);
							break;
						case 3:
							TxData[3] = strtol((uint8_t*)ptr2, NULL, 16);
							break;
						case 4:
							TxData[4] = strtol((uint8_t*)ptr2, NULL, 16);
							break;
						case 5:
							TxData[5] = strtol((uint8_t*)ptr2, NULL, 16);
							break;
						case 6:
							TxData[6] = strtol((uint8_t*)ptr2, NULL, 16);
							break;
						case 7:
							TxData[7] = strtol((uint8_t*)ptr2, NULL, 16);
							break;
						default:
							break;
					}
					idx2++;

					ptr2 = strtok(NULL, delim2);
    	    	}

    			//for (int i=0; i<8; i++)
    			//	TxData[i] = CanMsgData[i];
    		}
    		else
    		{

    		}
    		//printf("'%s'\n", ptr);
    		ptr = strtok(NULL, delim);

    		idx++;
    	}

    	HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan2, &TxHeader, &TxData[0]);
    	Ethernet_Notification_flag = 0;
    }
    else
    {
    	//HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan2, &TxHeader, &TxData[0]);
    	Ethernet_Notification_flag = 0;
    }

	//HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan2, &TxHeader, &TxData[8]);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_CanFDTask */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_CanFDTask */
void CanFDTask(void const * argument)
{
  /* USER CODE BEGIN CanFDTask */
	//const char* message = "Hello UDP message!\n\r";

	osDelay(1000);

	//ip_addr_t PC_IPADDR;
	//IP_ADDR4(&PC_IPADDR, 192, 168, 100, 2);

	/*struct udp_pcb* my_udp = udp_new();
	udp_connect(my_udp, &PC_IPADDR, 55151);
	struct pbuf* udp_buffer = NULL;*/

	/* Infinite loop */
	for (;;)
	{
	  osDelay(2500);
	  HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan2, &TxHeader, &TxData[0]);
	  HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan2, &TxHeader, &TxData[8]);

	}
  /* USER CODE END CanFDTask */
}

/* USER CODE BEGIN Header_LEDTask */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LEDTask */
void LEDTask(void const * argument)
{
  /* USER CODE BEGIN LEDTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(3300);
    HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
  }
  /* USER CODE END LEDTask */
}

/* MPU Configuration */

void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct = {0};

  /* Disables the MPU */
  HAL_MPU_Disable();
  /** Initializes and configures the Region and the memory to be protected
  */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.BaseAddress = 0x30040000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_32KB;
  MPU_InitStruct.SubRegionDisable = 0x0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL1;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_DISABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);
  /** Initializes and configures the Region and the memory to be protected
  */
  MPU_InitStruct.Number = MPU_REGION_NUMBER1;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256B;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_BUFFERABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);
  /* Enables the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);

}
/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
